package homework.map;

import java.util.HashMap;
import java.util.Map;

public class MyMap {

    public static void main(String[] args) {

        /*String message = "я кодю и кодю и кодю и кодю изучая Java";

        String[] words = message.split("\\s");

        Map<String, Integer> counterMap = new HashMap<>();

        for (String word : words) {

                Integer count = counterMap.get(word);

                if(count == null) {
                    count = 0;
                }
                counterMap.put(word, ++count);

            }

        for (String word : counterMap.keySet()) {

            System.out.println(word + ": " + counterMap.get(word));
        }*/

        String message = "я кодю и кодю и кодю и кодю изучая Java";

        String[] words = message.split("\\s");

        Map<String, Integer> counterMap = new HashMap<>();


        for (String word : words) {

            int count = 1;

            if (counterMap.containsKey(word)) {
                count = counterMap.get(word);
            }

            counterMap.put(word, ++count);
        }

        for (String word : counterMap.keySet()) {

            System.out.println(word + ": " + counterMap.get(word));
        }

    }

}








            /*if(true!=word.isEmpty()) {

                Integer count = counterMap.get(word);

                if(count == null) {

                    count = 0;
                }
                counterMap.put(word, ++count);
            }
        }

        for(String word : counterMap.keySet()) {

            System.out.println(word + ": " + counterMap.get(word));
        }
    }
}*/
