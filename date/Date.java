package homework.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.time.temporal.ValueRange;
import java.util.Calendar;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MINUTE_OF_DAY;
import static java.time.temporal.ChronoUnit.*;

public class Date {
    /*1) Различия между классами. Применение
    Класс LocalDate хранит год, месяц и день. Он используется для хранения и обработки даты без времени.

    Класс LocalDateTime — представляет собой дату и время в формате по умолчанию: yyyy-MM-dd-HH-mm-ss.zzz.

    Класс Calendar является абстрактным классом. предоставляет методы установки значений различных
    полей вроде YEAR, MONTH, DAY_OF_MONTH, HOUR и т. д, манипуляции этими полями,
    например добавление дня или месяца.New

2) Конструкторы

3) Методы LocalDate:
    https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html
     - format Форматирует эту дату с использованием указанного форматирования
 -  minus Возвращает копию этой даты с вычитаемой суммой.
            -  3 метода now Получает текущую дату с системных часов
 -  parse Получает экземпляр LocalDate из текстовой строки, такой как 2007-12-03.
            -  plus Возвращает копию этой даты с указанной добавленной суммой.
 -  of Получает экземпляр LocalDate из года, месяца и дня.
            -  until Вычисляет период между этой датой и другой датой в качестве Периода
 -  isAfter Проверяет, установлена ​​ли эта дата после указанной даты.
            -  isBefore Проверяет, установлена ​​ли эта дата до указанной даты.

            4) Методы LocalDateTime:
    https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html
            - atZone Объединяет эту дату-время с временной зоной для создания ZonedDateTime.
 - atOffset Объединяет эту дату-время со смещением для создания OffsetDateTime.
            - from Получает экземпляр LocalDateTime из временного объекта.
 -  truncatedTo Возвращает копию этого LocalDateTime с усеченным временем.
            -  range Возвращает диапазон допустимых значений для указанного поля.

            5) Методы Calendar:
    https://docs.oracle.com/javase/7/docs/api/java/util/Calendar.html
            - add Добавляет или вычитает указанный период времени в заданное поле календаря в соответствии с правилами календаря.
            - getInstance(TimeZone zone) Получает календарь, используя указанный часовой пояс и локаль по умолчанию.
            - getTimeZone() Возвращает часовой пояс
 - set Устанавливает заданное поле календаря на заданное значение.

            6) Примеры кода использования методов. Получение из этих объектов, объект Date*/

    public static void main(String[] args) {
        /*Методы LocalDate:*/

        /*- format Форматирует эту дату с использованием указанного форматирования*/
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM yyyy dd");
        System.out.println(date.format(formatter));

       /* minus Возвращает копию этой даты с вычитаемой суммой.*/

        System.out.println(date.minusDays(1));

       /* parse Получает экземпляр LocalDate из текстовой строки, такой как 2007-12-03.*/


        System.out.println(LocalDate.parse("2007-12-03"));

        /*of Получает экземпляр LocalDate из года, месяца и дня.*/
        LocalDate newDate = LocalDate.of(2011, 9, 5);

        System.out.println(newDate);

        Period period = Period.between (date, newDate);
        System.out.println("Period " + period);


        System.out.println(date.isAfter(newDate));

        System.out.println(date.isBefore(newDate));


        /*4) Методы LocalDateTime:*/

        LocalDateTime dateL = LocalDateTime.now();
        dateL.atZone(ZoneOffset.of("GMT + 2"));
        System.out.println(dateL);


//        System.out.println(dateL.atOffset(ZoneOffset.of("+2")));
        System.out.println(dateL.atOffset(ZoneOffset.of("+2")));

        /*- from Получает экземпляр LocalDateTime из временного объекта.*/

        System.out.println(dateL);
        System.out.println(dateL.truncatedTo(MINUTES));

        ValueRange valueRange = dateL.range(MINUTE_OF_DAY);

        System.out.println(valueRange);


        /*5) Методы Calendar:*/

        Calendar rightNow = Calendar.getInstance();

        System.out.println(rightNow);

        rightNow.add(Calendar.DAY_OF_MONTH, -5);

        System.out.println(rightNow.getTimeZone());

        rightNow.set(Calendar.DAY_OF_MONTH,1);

        System.out.println(rightNow);
    }

}
