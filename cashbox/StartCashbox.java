package homework.cashbox;

/*Касса ж/д вокзала.
        Создать приложение Касса ж/д вокзала. Изначально имеется 10 билетов.
        Некоторые покупатели хотят купить билеты, а  некоторые сдать билеты.
        Если билетов нехватает покупатели ждут пока сдадут билеты. Выводить текст - "Все билеты проданы".
        Если билет сдается необходимо опевещать покупателей.
        Если билет сдан - выводить текст "Билет сдан покупателем + имя покупателя"
        Покупатели и сдатчики имеют имена.
        Если билет куплен - выводить текст "Билет куплен покупателем + имя покупателя".*/

import java.util.ArrayList;
import java.util.List;

public class StartCashbox {
    public static void main(String[] args) {

        List<Integer> taskQueue = new ArrayList<Integer>();

        int MAX_CAPACITY = 10;

        Thread tAlex = new Thread(new Alex(taskQueue, MAX_CAPACITY), "Alex");

        Thread tVitold = new Thread(new Vitold(taskQueue), "Vitold");

        tAlex.start();

        tVitold.start();

    }
}


class Alex implements Runnable {
    private final List<Integer> taskQueue;
    private final int MAX_CAPACITY;

    public Alex(List<Integer> sharedQueue, int size) {
        this.taskQueue = sharedQueue;

        this.MAX_CAPACITY = size;
    }

    @Override
    public void run() {
        int counter = 1;

        while (true) {
            try {
                deserter(counter);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void deserter(int i) throws InterruptedException {
        synchronized (taskQueue) {
            while (taskQueue.size() == MAX_CAPACITY) {

                taskQueue.wait();
            }

            Thread.sleep(1000);

            taskQueue.add(i);

            System.out.println("Ticket is passed.  Customer = " + Thread.currentThread().getName());

            taskQueue.notifyAll();
        }
    }
}


class Vitold implements Runnable {
    private final List<Integer> taskQueue;

    public Vitold(List<Integer> sharedQueue) {
        this.taskQueue = sharedQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                customer();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void customer() throws InterruptedException {
        synchronized (taskQueue) {
            while (taskQueue.isEmpty()) {
                System.out.println("All tickets is sold");
                taskQueue.wait();
            }

            Thread.sleep(1000);

            int i = taskQueue.remove(0);

            System.out.println("Ticket is bought. Customer: " + Thread.currentThread().getName());

            taskQueue.notifyAll();
        }
    }
}