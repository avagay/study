package homework.jdbc;

import java.sql.*;
import java.util.Objects;

/*
Создать имитацию запросов в базу данных на основе коллекции c применением lambdas.

Сущности Man(имя, фамилия, возраст, количество детей, Адрес), Адрес(страна, город, улица, количество жителей)

Использовать коллекцию ArrayList.

"SELECT * FROM Man";

 "SELECT * FROM Address";

 "SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";

 "UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)

 "SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";

 "SELECT count(*) FROM Address GROUP BY city, nameOfStreet";

 "SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";

 "SELECT count(*) FROM Man GROUP BY city, nameOfStreet";

 "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 10";*/

public class MyJDBC {
    private static Connection connection;

    public static final String URL = "jdbc:mysql://localhost:3306/home?user=root&password=root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        if (Objects.isNull(connection)) {
            try {
                connection = DriverManager.getConnection(URL);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return connection;
    }


    /*"SELECT * FROM Man";*/
    public void selectAllFromMan() {
        try (Connection connection = DriverManager.getConnection(URL);

             Statement statement = connection.createStatement();

             ResultSet resultSet = statement.executeQuery("SELECT * FROM man")) {
            while (resultSet.next()) {
                int manId = resultSet.getInt("man_id");

                String firstName = resultSet.getString("first_name");

                String secondName = resultSet.getString("second_name");

                int age = resultSet.getInt("age");

                int countChild = resultSet.getInt("count_child");

                System.out.println(manId + "\t" + firstName + "\t" + secondName
                        + "\t" + age + "\t" + countChild);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*"SELECT * FROM Address";*/

    public void SelectAllFromAddress() {
        try (Connection connection = DriverManager.getConnection(URL);

             Statement statement = connection.createStatement();

             ResultSet resultSet = statement.executeQuery("SELECT * FROM man_address")) {

            while (resultSet.next()) {
                int manAddressId = resultSet.getInt("man_address_id");

                String country = resultSet.getString("country");

                String city = resultSet.getString("city");

                String street = resultSet.getString("street");

                int count_citizens = resultSet.getInt("count_citizens");

                System.out.println(manAddressId + "\t" + country +
                        "\t" + city + "\t" + street + "\t" + count_citizens);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /*"SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";*/


    public void SelectFromManByAge(int age) throws SQLException {

        String query = "SELECT first_name, second_name, count_child " +
                "FROM man " +
                "WHERE age >= ? " +
                "ORDER BY first_name";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            preparedStatement.setInt(1, age);

            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");

                String secondName = resultSet.getString("second_name");

                int countChild = resultSet.getInt("count_child");

                System.out.println(firstName + "\t" + secondName + "\t" + countChild);
            }
        }
    }

    /*""SELECT firstName, lastName, nameOfStreet FROM Man
    WHERE country == 'Canada' AND count_child == 3 OR age >= 25";*/

    public void SelectFromManByCountryAndChildOrAge(String country, int countChild, int age)
            throws SQLException {

        String query = "SELECT first_name, second_name, ma.street " +
                "FROM man m " +
                "INNER JOIN man_address ma " +
                "ON m.man_address_fk = ma.man_address_id " +
                "WHERE ma.country = ? AND m.count_child = ? OR m.age >= ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, country);
            preparedStatement.setInt(2, countChild);
            preparedStatement.setInt(3, age);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String secondName = resultSet.getString("second_name");
                String street = resultSet.getString("street");

                System.out.println(firstName + "\t" + secondName + "\t" + street);
            }
        }
    }

    /*"UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3
    WHERE country == 'US' (or another country)*/


    public void updateManByCountry(String country, String newFirstName, String newSecondName,
                                   int newCountChild) throws SQLException {

        String query = "UPDATE man m " +
                "INNER JOIN man_address ma " +
                "ON m.man_address_fk = ma.man_address_id " +
                "SET first_name = ?, second_name = ?, count_child = ? " +
                "WHERE ma.country = ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {

            preparedStatement.setString(1, newFirstName);
            preparedStatement.setString(2, newSecondName);
            preparedStatement.setInt(3, newCountChild);
            preparedStatement.setString(4, country);

            preparedStatement.executeUpdate();

            System.out.println("UPDATE is finished");
        }

        /*try (Connection connection = DriverManager.getConnection(URL);

             Statement statement = connection.createStatement()) {
             statement.executeUpdate("UPDATE man m " +
                    "INNER JOIN man_address ma " +
                    "ON m.man_address_fk = ma.man_address_id " +
                    "SET first_name = ?, second_name = ?, count_child = ? " +
                    "WHERE ma.country = ?");

        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }

    /*"SELECT count(*) FROM Address GROUP BY city, nameOfStreet";*/

    public void countFromAddressGroupBy() throws SQLException {

        try (Connection connection = DriverManager.getConnection(URL);

             Statement statement = connection.createStatement();

             ResultSet resultSet = statement.executeQuery("SELECT count(*), city, street FROM man_address GROUP BY city, street")) {

            while (resultSet.next()) {
                int count = resultSet.getInt("count(*)");

                String city = resultSet.getString("city");

                String street = resultSet.getString("street");

                System.out.println(count + "\t" + city + "\t" + street);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*"SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";*/

    public void countFromAddressGroupHaving(int countCitizens)
            throws SQLException {

        String query = "SELECT count(*), city, street, count_citizens " +
                "FROM man_address GROUP BY city, street HAVING count_citizens > ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, countCitizens);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int count = resultSet.getInt("count(*)");

                String city = resultSet.getString("city");

                String street = resultSet.getString("street");

                System.out.println(count + "\t" + city + "\t" + street);
            }
        }
    }

    /*"SELECT count(*) FROM Man GROUP BY city, nameOfStreet"*/

    public void countFromManGroupBy() throws SQLException {

        try (Connection connection = DriverManager.getConnection(URL);

             Statement statement = connection.createStatement();

             ResultSet resultSet = statement.executeQuery("SELECT count(*), city, street FROM man m " +
                     "INNER JOIN man_address ma ON m.man_address_fk = ma.man_address_id GROUP BY city, street")) {

            while (resultSet.next()) {
                int count = resultSet.getInt("count(*)");

                String city = resultSet.getString("city");

                String street = resultSet.getString("street");

                System.out.println(count + "\t" + city + "\t" + street);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*"SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 10"*/

    public void countFromManGroupHaving(int countCitizens)
            throws SQLException {

        String query = "SELECT count(*), city, street, count_citizens FROM man m " +
                "INNER JOIN man_address ma " +
                "ON m.man_address_fk = ma.man_address_id " +
                "GROUP BY city, street HAVING count_citizens > ?";

        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, countCitizens);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int count = resultSet.getInt("count(*)");

                String city = resultSet.getString("city");

                String street = resultSet.getString("street");

                System.out.println(count + "\t" + city + "\t" + street);
            }
        }
    }

}


    class TestMyJDBC {
        public static void main(String[] args) throws SQLException {

            MyJDBC connector = new MyJDBC();

        /*connector.selectAllFromMan();*/

        /*connector.SelectAllFromAddress();*/

     /*   try {
            connector.SelectFromManByAge(20);
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

      /*  try {
            connector.SelectFromManByCountryAndChildOrAge("Moskow",2,30);
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        /*connector.updateManByCountry("Franch", "Johnisss", "Kennedi",
        3);*/

        /*connector.countFromAddressGroupBy();*/

        /*connector.countFromAddressGroupHaving(500000);*/

        /*connector.countFromManGroupBy();*/

        /*connector.countFromManGroupHaving(500000);*/

            MyJDBC.getConnection().close();

        }
    }

