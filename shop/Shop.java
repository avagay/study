package homework.shop;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
2) Создать приложение интернет - магазина. Приложение должен выполнять следующие операции:

        1) добавление товара(название, производитель, дата производства и т.д.).
        2) добавление сопутствующих товаров.
        3) удаление товара с сопутствующими товарами.
        4) создание корзины для каждого юзера.
        5) выбор товара по определенным критериям и после выводить сопутствующие товары.

        Примеры товаров: мобильные телефоны, телевизоры и т.д.

        В приложении применить коллекции, lambdas + для расчета корзины применить функциональный интерфейс

*/


public class Shop {

    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        int operation;

        ShopService shopService = new ShopService();

        do {
            System.out.println("Add goods and related goods - 1 ,'\n" +
                    "Show goods - 2 , \n" +
                    "Remove good - 3, \n" +
                    "Search of goods by name + related products  - 4, \n" +
                    "Add product to cart - 5, \n" +
                    "Show cart - 6, \n" +
                    "Creat user - 7, \n" +
                    "Exit - 0");
            operation = SCANNER.nextInt();


            switch (operation) {
                case 1:

                    String category = getInfoFromUser("Select product category : " +
                            Arrays.toString(Category.values())).toUpperCase();

                    String manufacturer = getInfoFromUser("Select product manufacturer : " +
                            Arrays.toString(Manufacturer.values())).toUpperCase();

                    String type = getInfoFromUser("Select product type : " +
                            Arrays.toString(Type.values())).toUpperCase();

                    String fullName = getInfoFromUser("Select product name : ").toUpperCase();

                    int price = Integer.parseInt(getInfoFromUser("Select product price : "));

                    shopService.addNewGoods(new Product(Category.valueOf(category),
                            Manufacturer.valueOf(manufacturer), Type.valueOf(type),
                            fullName, price));

                    break;
                case 2:
//                    Показать все товары и все сопутсвующие товары
                    shopService.showGoods();

                    break;
                case 3:
//                    Удаление товаров с сопуствующими товарами по имени
                    String name = getInfoFromUser("Select product name : ").toUpperCase();
                    shopService.removeGoods(name);
                    break;
                case 4:
//                  Поиск по имени товара
                    shopService.searchProduct(getInfoFromUser("Select product name : ").toUpperCase());
                    break;
                case 5:
//                  Добавление в корзину
                    shopService.addProductToCart(getInfoFromUser("Select product name : ").toUpperCase(),
                            getInfoFromUser("Enter user name : ").toUpperCase());
                    break;
                case 6:
//                  Показать корзину
                    shopService.showCart(getInfoFromUser("Enter user name : ").toUpperCase());
                    break;

                case 7:
//                  создать пользователя
                    shopService.createUser(getInfoFromUser("Enter user name : ").toUpperCase());

                    break;


            }
        }
        while (operation != 0);
    }

    public static String getInfoFromUser(String message) {

        System.out.println(message);

        return SCANNER.next();

    }

}

class ShopService {

    private Map<String, List<Product>> listMap = new HashMap<>();

    static private List<Product> products;

    public ShopService() {
        products = new LinkedList<>();
    }


    public void addNewGoods(Product product) {

        products.add(product);

        System.out.println("Added  " + product);

    }

    public void showGoods() {

        products.stream().forEach(System.out::println);
    }

    public void removeGoods(String name) {

        Predicate<Product> predicate = product -> product.getFullName().equals(name);

       /* Product product1 = products.stream()
                .filter(product -> product.getFullName().equals(name))
                .findFirst().get();*/


        /*products.remove(product1);*/
        products.removeIf(predicate);

    }

    public void searchProduct(String name) {

        Product mainProduct = products.stream()
                .filter(product -> product.getFullName().equals(name))
                .collect(Collectors.toList()).get(0);

        System.out.println("Main good = " + mainProduct);

        products.stream()
                .filter(product -> product.searchEquals(mainProduct))
                .forEach(product -> System.out.println("Accompanying good = " + product));

    }

    public void createUser(String name) {

        List<Product> cartProducts = new LinkedList<>();

        listMap.put(name, cartProducts);
    }


    public void addProductToCart(String name, String userName) {

        Product searchedProduct = products.stream()
                .filter(product -> product.getFullName().equals(name))
                .findFirst()
                .get();
        listMap.get(userName).add(searchedProduct);

    }

    public void showCart(String userName) {

        Calculate culculeCart = () -> {
            int sum = 0;

            for (Product product : listMap.get(userName)) {
                System.out.println(product);
                sum += product.getPrice();
            }

            return sum;
        };


        System.out.println(culculeCart.calculateSum());

    }

}

 /*
    * List<Product> products = listMap.get(userName);

        products.stream().forEach(System.out::println);

        Сulcule culculeCart = (items) -> items.stream().mapToInt(Product::getPrice).sum();

        System.out.println(culculeCart.culculeSum(products))
    * */

class Product {

    private Category category;

    private Manufacturer manufacturer;

    private Type type;

    private String fullName;

    private int price;

    public Product(Category category, Manufacturer manufacturer, Type type, String fullName, int price) {

        this.category = category;
        this.manufacturer = manufacturer;
        this.type = type;
        this.fullName = fullName;
        this.price = price;

    }

    public String getFullName() {
        return fullName;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product = " + fullName;
    }

    public boolean searchEquals(Product product) {

        if (category != product.category) return false;
        if (manufacturer != product.manufacturer) return false;
        if (type == product.type) return false;
        if (fullName == product.fullName) return false;
        else return true;
    }

}

enum Category {
    MOBILE("Mobile"), TV("TV"), PC("PC");

    private String shortName;

    Category(String shortName) {

        this.shortName = shortName;

    }

}

enum Manufacturer {
    SAMSUNG("Samsung"), APPLE("Apple");

    private String shortName;

    Manufacturer(String shortName) {

        this.shortName = shortName;

    }
}

enum Type {
    LAPTOP("Laptop"), HEADPHONES("Headphones");

    private String shortName;

    Type(String shortName) {

        this.shortName = shortName;

    }
}

@FunctionalInterface
interface Calculate {
    int calculateSum();
}




