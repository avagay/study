//package homework.file_manager;
//
///*1) Создать приложение файловый менеджер.
//        Приложение должно позволять:
//        1) Создавать текстовые файлы и директории
//        2) Конвертацию текстовых файлов в PDF файлы с помощью библиотеки
//        iText(http://howtodoinjava.com/apache-commons/create-pdf-files-in-java-itext-tutorial/#itext_overview)
//        3) Копировать файлы из одной директории в другую, если такой файл уже существует - перезатирать его
//        4) Удалять дирекотрии и файлы
//        5) Переименовывать директории и файлы
//        6) Просматривать содержимое директории*/
//
//
//import com.itextpdf.text.DocumentException;
//
//import java.io.IOException;
//import java.nio.file.*;
//import java.nio.file.attribute.BasicFileAttributes;
//import java.util.Scanner;
//
//public class FileManager {
//
//    public static final Scanner SCANNER = new Scanner(System.in);
//
//    public static void main(String[] args) {
//        try {
//            int operation;
//
//            do {
//                System.out.println("Create text file - 1, " +
//                        "Create directory - 2, " +
//                        "Convert to PDF - 3, " +
//                        "Copy files from one directory to another - 4, " +
//                        "Delete directory - 5, " +
//                        "Delete file - 6, " +
//                        "Rename directory - 7 " +
//                        "Rename file - 8, " +
//                        "View the contents of the directory - 9 , " +
//                        "exit - 0 ");
//                operation = SCANNER.nextInt();
//                OperationFile operationFile = new OperationFile();
//
//                switch (operation) {
//                    case 1:
//                        Path path = Paths.get(SCANNER.nextLine());
//                        operationFile.createTextFile(path);
//                        break;
//                    case 2:
//                        path = Paths.get(SCANNER.nextLine());
//                        operationFile.createDirectory(path);
//                        break;
//                    case 3:
//                        String name = SCANNER.nextLine();
//                        operationFile.createPDF(Paths.get(name));
//                        break;
//                    case 4:
//                        Path source = Paths.get(SCANNER.nextLine());
//                        Path target = Paths.get(SCANNER.nextLine());
//                        operationFile.copyFile(source, target);
//                        break;
//                    case 5:
//                        path = Paths.get(SCANNER.next());
//                        operationFile.deleteDirectory(path);
//                        break;
//                    case 6:
//                        path = Paths.get(SCANNER.nextLine());
//                        operationFile.deleteFile(path);
//                        break;
//                    case 7:
//                        source = Paths.get(SCANNER.nextLine());
//                        target = Paths.get(SCANNER.nextLine());
//                        operationFile.renameDirectory(source, target);
//                        break;
//                    case 8:
//                        source = Paths.get(SCANNER.nextLine());
//                        target = Paths.get(SCANNER.nextLine());
//                        operationFile.renameFile(source, target);
//                        break;
//                    case 9:
//                        path = Paths.get(SCANNER.nextLine());
//                        operationFile.viewDirectory(path);
//                        break;
//                }
//            }
//            while (operation != 0);
//        } catch (IOException | DocumentException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
//
//class MyFileVisitor extends SimpleFileVisitor<Path> {
//
//    @Override
//    public FileVisitResult visitFile(Path path,
//                                     BasicFileAttributes fileAttributes) {
//        System.out.println("file name:" + path.getFileName());
//        return FileVisitResult.CONTINUE;
//    }
//
//    @Override
//    public FileVisitResult preVisitDirectory(Path path,
//                                             BasicFileAttributes fileAttributes) {
//        System.out.println(path);
//        return FileVisitResult.CONTINUE;
//    }
//}
//
//class MyFileVisitorForDelete extends SimpleFileVisitor<Path> {
//
//    @Override
//    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
//        Files.delete(dir);
//        return FileVisitResult.TERMINATE;
//    }
//
//    @Override
//    public FileVisitResult visitFile(Path path,
//                                     BasicFileAttributes fileAttributes) throws IOException {
//        Files.delete(path);
//        System.out.println("file was deleted:" + path.getFileName());
//        return FileVisitResult.CONTINUE;
//    }
//
//}
//
//
//
//
