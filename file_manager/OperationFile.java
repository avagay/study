//package homework.file_manager;
//
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Paragraph;
//import com.itextpdf.text.pdf.PdfWriter;
//
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//
//public class OperationFile {
//
//    public static final String EXTENSION_PDF = ".pdf";
//
//    public void createDirectory(Path path) throws IOException {
//        Files.createDirectories(path);
//    }
//
//    public void createTextFile(Path path) throws IOException {
//        Files.createFile(path);
//    }
//
//
//    public void createPDF(Path path) throws DocumentException, IOException {//correct handling exception
//        String content = new String(Files.readAllBytes(path));
//
//        Document document = new Document();
//
//        String name = path.toFile().getName();
//        String p = name.substring(0, name.indexOf(".txt"));
//
//        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(p + EXTENSION_PDF));
//
//        document.open();
//
//        document.add(new Paragraph(content));
//
//        document.close();
//
//        writer.close();
//    }
//
//    public void copyFile(Path source, Path target) throws IOException {
//        Files.copy(source, target);
//    }
//
//    public void deleteDirectory(Path path) throws IOException {//walkFileTree
//        Files.walkFileTree(path, new MyFileVisitorForDelete());
////        Files.delete(path);
//    }
//
//    public void deleteFile(Path path) throws IOException {
//        Files.delete(path);
//    }
//
//    public void renameFile(Path source, Path target) throws IOException {
//        Files.move(source, target);
//    }
//
//    public void renameDirectory(Path source, Path target) throws IOException {
//        Files.move(source, target);
//    }
//
//
//    public void viewDirectory(Path path) throws IOException {
//        Files.walkFileTree(path, new MyFileVisitor());//walkFileTree
//
//    }
//}
