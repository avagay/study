package homework.patterns.sql_builder;

/*1) Использовать шаблон "Строитель"
для построения SQL запросов(SELECT, UPDATE) из задачи Имитация запросов в базу данных.

 "SELECT * FROM Man";

         "SELECT * FROM Address";

         "SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";

         "UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)

         "SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";

         "SELECT count(*) FROM Address GROUP BY city, nameOfStreet";

         "SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";

         "SELECT count(*) FROM Man GROUP BY city, nameOfStreet";

         "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 10";*/

import java.util.stream.IntStream;

public class SelectBuilder {

    private StringBuilder request;


    public SelectBuilder() {
        this.request = new StringBuilder("UPDATE ");
    }

    SelectBuilder buildArgs(String... args) {

        request.append(args[0]);

        IntStream.range(1, args.length).forEach(ar -> {
            request.append(", ").append(args[ar]);
        });
        return this;
    }

    SelectBuilder buildFromClause(String nameOfTable) {
        request.append(" FROM ").append(nameOfTable);
        return this;
    }

    SelectBuilder buildConditions(String... conditions) {
        request.append(" WHERE ").append(conditions[0]);

        IntStream.range(1, conditions.length).forEach(ar -> {
            request.append(", ").append(conditions[ar]);
        });
        return this;
    }

    SelectBuilder buildSort(String nameOfField) {
        request.append(" ORDER BY ").append(nameOfField);
        return this;
    }

    SelectBuilder buildCount(){

        request.append(" count(*) ");

        return this;
    }

    SelectBuilder buildGROUP(String... namesOfGroup){

        request.append(" GROUP BY ").append(namesOfGroup[0]);

        IntStream.range(1, namesOfGroup.length).forEach(ar -> {
            request.append(", ").append(namesOfGroup[ar]);
        });

        return this;
    }

    SelectBuilder buildHaving(String...namesOfField){

        request.append(" HAVING ").append(namesOfField[0]);

        IntStream.range(1, namesOfField.length).forEach(ar -> {
            request.append(namesOfField[ar]);
        });

        return this;
    }


    public String build() {
        return request.toString();
    }



    public static void main(String[] args) {

        /*"SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";*/

        String query = new SelectBuilder()
                .buildArgs("first_name", "last_name", "count_childrens")
                .buildFromClause("Man")
                .buildConditions(" age >= 20 ")
                .buildSort("first_name")
                .build();

        /*"SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";*/

        query = new SelectBuilder()
                .buildArgs("first_name", "last_name", "count_childrens")
                .buildFromClause("Man").buildConditions(" country == 'Canada' AND numberOfHome == 3 OR age >= 25 ")
                .build();


        /*"SELECT count(*) FROM Address GROUP BY city, nameOfStreet";*/

        query = new SelectBuilder()
                .buildCount()
                .buildFromClause("Address")
                .buildGROUP("city","nameOfStreet")
                .build();



        /*"SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";*/

        query = new SelectBuilder()
                .buildCount()
                .buildFromClause("Address")
                .buildGROUP("city","nameOfStreet")
                .buildHaving("countOfCitizens > 10")
                .build();


        /*"SELECT count(*) FROM Man GROUP BY city, nameOfStreet";*/

        query = new SelectBuilder()
                .buildCount()
                .buildFromClause("Man")
                .buildGROUP("city","nameOfStreet")
                .build();




        /*"SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 10";*/

        query = new SelectBuilder()
                .buildCount()
                .buildFromClause("Man")
                .buildGROUP("city","nameOfStreet")
                .buildHaving("countOfCitizens > 10")
                .build();

        System.out.println(query);
    }
}

class UpdateBuilder {

    private StringBuilder request;


    public UpdateBuilder() {
        this.request = new StringBuilder("UPDATE ");
    }

    UpdateBuilder buildArgs(String... args) {

        request.append(args[0]);

        IntStream.range(1, args.length).forEach(ar -> {
            request.append(", ").append(args[ar]);
        });
        return this;
    }

    UpdateBuilder buildFromClause(String nameOfTable) {
        request.append(" FROM ").append(nameOfTable);
        return this;
    }

    UpdateBuilder buildConditions(String... conditions) {
        request.append(" WHERE ").append(conditions[0]);

        IntStream.range(1, conditions.length).forEach(ar -> {
            request.append(", ").append(conditions[ar]);
        });
        return this;
    }

    UpdateBuilder buildSort(String nameOfField) {
        request.append(" ORDER BY ").append(nameOfField);
        return this;
    }

    UpdateBuilder buildCount() {

        request.append(" count(*) ");

        return this;
    }

    UpdateBuilder buildGROUP(String... namesOfGroup) {

        request.append(" GROUP BY ").append(namesOfGroup[0]);

        IntStream.range(1, namesOfGroup.length).forEach(ar -> {
            request.append(", ").append(namesOfGroup[ar]);
        });

        return this;
    }

    UpdateBuilder buildHaving(String... namesOfField) {

        request.append(" HAVING ").append(namesOfField[0]);

        IntStream.range(1, namesOfField.length).forEach(ar -> {
            request.append(namesOfField[ar]);
        });

        return this;
    }

    UpdateBuilder buildSet(String namesOfField) {

        request.append(" SET ").append(namesOfField);

        return this;
    }


    public String build() {
        return request.toString();
    }

    public static void main(String[] args) {

        /*UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US'*/

        String query = new UpdateBuilder()
                .buildFromClause("Man")
                .buildSet("firstName = 'John', lastName = 'Kennedi', countOfChildren = 3")
                .buildConditions("country == 'US'")
                .build();

        System.out.println(query);

    }

}

