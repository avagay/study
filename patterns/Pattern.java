package homework.patterns;

public class Pattern {
    public static void main(String[] args) {

        Figures figures = new FiguresWithCross();

        figures.print();

        figures = new FiguresWithSquare();

        figures.print();

        figures = new FiguresWithLine();

        figures.print();

    }

}


abstract class Figures {

    protected final void print() {

        printFigures();

        printAnotherFigures();

        printFigures();

    }


    private void printFigures() {

        System.out.println("    *\n" +
                "   ***\n" +
                "  *****\n" +
                " *******\n" +
                "*********\n");

    }


    public abstract void printAnotherFigures();

}

class FiguresWithCross extends Figures {

    @Override
    public void printAnotherFigures() {

        System.out.println("*********\n");

    }
}

class FiguresWithSquare extends Figures {

    @Override
    public void printAnotherFigures() {

        System.out.println(" *******\n" +
                " *******\n" +
                " *******\n" +
                " *******\n");

    }
}

class FiguresWithLine extends Figures {
    
    @Override
    public void printAnotherFigures() {

        System.out.println("   ***\n" +
                "   ***\n" +
                "**********\n" +
                "**********\n" +
                "   ***\n" +
                "   ***\n");

    }
}





