package homework.patterns;

 /*2) Добавить функционал добавления к строке \ и {} к шаблону Декоратор, чтобы получить строку \{"Hello"}\*/

public class MyDecorator {

    public static void main(String[] args) {

        QuotesDecorator decorator = new QuotesDecorator(
                new LeftRoundBracketDecorator(
                        new RightRoundBracketDecorator(
                                new LeftBracketDecorator(
                                    new RightBracketDecorator(
                                new RealPrinter("Hello"))))));

        decorator.print();
    }

}

interface Printable {
    void print();
}


class RealPrinter implements Printable {

    private String text;

    public RealPrinter(String text) {
        this.text = text;
    }

    @Override
    public void print() {
        System.out.print(text);
    }
}


class LeftBracketDecorator implements Printable {

    private Printable printable;

    public LeftBracketDecorator(Printable printable) {
        this.printable = printable;
    }


    @Override
    public void print() {
        System.out.print("\"");

        printable.print();
    }
}


class RightBracketDecorator implements Printable {

    private Printable printable;

    public RightBracketDecorator(Printable printable) {
        this.printable = printable;
    }

    @Override
    public void print() {
        printable.print();

        System.out.print("\"");
    }
}

class LeftRoundBracketDecorator implements Printable {

    private Printable printable;

    public LeftRoundBracketDecorator(Printable printable) {
        this.printable = printable;
    }


    @Override
    public void print() {
        System.out.print("{");

        printable.print();
    }
}


class RightRoundBracketDecorator implements Printable {

    private Printable printable;

    public RightRoundBracketDecorator(Printable printable) {
        this.printable = printable;
    }

    @Override
    public void print() {
        printable.print();

        System.out.print("}");
    }
}

class QuotesDecorator implements Printable {

    private Printable printable;

    public QuotesDecorator(Printable printable) {
        this.printable = printable;
    }

    @Override
    public void print() {

        System.out.print("\\");

        printable.print();

        System.out.print("\\");
    }
}
