package homework.translator;

/*Создать приложение переводчик.

        Приложение должно позволять:
        - добавлять новые слова.
        - переводить предложение с одного из языков(
        Русский - Немецкий,
        Немецкий - Русский,
        Английский - Русский,
        Русский - Английский,
        Английский - Испанский,
        Испанский - Английский).
        - смену языка перевода.
        - после ввода определять язык, на котором ввел юзер
        1) eng_rus.txt -> Hello:Привет
        1) de_rus.txt -> Hello:Привет
        1) eng_rus.txt -> Hello:Привет
        2) Map<String(eng_rus), Map<String(Hello), String", Hello:Привет, "de_rus", "Guten tak":Доброе утро


        Map<String(), Map<String, String>>
        */

import sun.plugin2.gluegen.runtime.StructAccessor;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static homework.translator.ServiceTranslator.PATH;


public class Translator {

    public static void main(String[] args) throws IOException {

        ServiceTranslator controllerTranslator = new ServiceTranslator();

        controllerTranslator.startTranslator(PATH);

        controllerTranslator.translate("Hello", "eng_rus");

        controllerTranslator.specifyLanguage("Привет");

    }

}


class ServiceTranslator {

    public static final Path PATH = Paths.get("./translator");

    private static final String UNDERSCORE = "_";

    private Map<String, Map<String, String>> listMap;//eng_rus -> Hello:Привет


    public ServiceTranslator() {
        listMap = new HashMap<>();
    }

    public void startTranslator(Path path) throws IOException {

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {

                List<String> lines = Files.readAllLines(path, Charset.forName("windows-1251"));//Hello:Привет

                String name = path.toFile().getName();

                int positionDot = name.indexOf(".");

                createTranslators(name.substring(0, positionDot), lines);

                return FileVisitResult.CONTINUE;
            }
        });

    }

    private void createTranslators(String fileName, List<String> lines) {
        //create right and reverse maps

        OperationString operationString = () -> {

            Map<String, String> wordsRight = new HashMap<>();

            Map<String, String> wordsReverse = new HashMap<>();

            String[] languages = fileName.split(UNDERSCORE);

            lines.stream().forEach(line -> {

                String[] contentWords = line.split(":");

                wordsRight.put(contentWords[0], contentWords[1]);

                wordsReverse.put(contentWords[1], contentWords[0]);
            });

            listMap.put(fileName, wordsRight);//eng_rus, eng_de

            listMap.put(languages[1] + UNDERSCORE + languages[0], wordsReverse);//rus_eng

        };

        operationString.operation();

    }

    public void translate(String sentence, String language) {

        Stream<String> stream = Arrays.stream(sentence.split("\\s"));

        StringBuilder sentenceTranslate = new StringBuilder();

        Map<String, String> map = listMap.get(language);

        stream.forEach(word -> {

            if (!map.containsKey(word)) {
                sentenceTranslate.append("!!!unknow word!!! -> ").append(word);
                return;
            }

            sentenceTranslate.append(" ").append(map.get(word));

        });

        System.out.println(sentenceTranslate);
    }

    public void specifyLanguage(String word) {

        String language = listMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue().containsKey(word))
                .findFirst()
                .get()
                .getKey();

        System.out.println(language.substring(0, language.indexOf(UNDERSCORE)));

    }

}

@FunctionalInterface
interface OperationString {
    void operation();
}




