
package homework.counter_reading;


/*
#7) Есть файл с показаниями и есть файл с тарифами. Сгенерировать PDF с отчетом о потраченных ресурсах + стоимость
        Reading
        01.09.17 - газ: 2083, электричество: 1098, вода: 3089.
        01.10.17 - газ: 2183, электричество: 1198, вода: 3189.

        Rates
        газ - 10 руб
        электричество - 5 руб
        вода - 2 руб

        Создать меню.
*/


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import static homework.counter_reading.RateUtil.*;
import static homework.file_manager.OperationFile.EXTENSION_PDF;

public class Counter {

    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) throws IOException, DocumentException {

        int operation;

        OperationCounter counter = new OperationCounter();

        int rateElectricity = counter.getDifferenceRetes(FROM_ELECTRICITY, TO_ELECTRICITY);

        int countElectricity = counter.getDifferenceReadings(37, 41, 93, 97);

        int rateGas = counter.getDifferenceRetes(6, 8);

        int countGas = counter.getDifferenceReadings(16, 20, 72, 76);

        int rateWater = counter.getDifferenceRetes(44, 45);

        int countWater = counter.getDifferenceReadings(49, 53, 105, 109);


        do {
            System.out.println("Conclusion electricity calculation - 1, " +
                    "Conclusion gas calculation - 2, " +
                    "Conclusion water calculation - 3 " +
                    "Generate PDF - 4, " +
                    "exit - 0 ");
            operation = SCANNER.nextInt();

            switch (operation) {
                case 1:
                    counter.printRateCount(rateElectricity, countElectricity);

                    break;
                case 2:
                    counter.printRateCount(rateGas, countGas);

                    break;
                case 3:
                    counter.printRateCount(rateWater, countWater);

                    break;
                case 4:
                    counter.generatePDF(countElectricity, rateElectricity, countGas, rateGas, countWater, rateWater);

                    break;
            }
        }
        while (operation != 0);
    }

}

class OperationCounter {

    private String readings;
    private String rates;

    {
        try {
            readings = new String(Files.readAllBytes(Paths.get(PATH_TO_RATES)));
            rates = new String(Files.readAllBytes(Paths.get(PATH_TO_READINGS)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void printRateCount(int rate, int count) {
        System.out.println(YOU_SPENT + count +
                RATE + rate + РУБ + YOU_NEED_PAY + calculationPayment(rate, count) + РУБ);
    }


    public int calculationPayment(int rate, int count) {

        return rate * count;
    }

    public void generatePDF(int countElectricity, int rateElectricity, int countGas, int rateGas, int countWater,
                            int rateWater) throws FileNotFoundException, DocumentException {

        String content = YOU_SPENT + ELECTRICITY + countElectricity + RATE +
                rateElectricity + РУБ + YOU_NEED_PAY + calculationPayment(rateElectricity, countElectricity) + РУБ +
                "\n" + YOU_SPENT + GAS + countGas + RATE + rateGas + РУБ + YOU_NEED_PAY +
                calculationPayment(rateGas, countGas) + РУБ + "\n" + YOU_SPENT + WATER + countWater + RATE +
                rateWater + РУБ + YOU_NEED_PAY + calculationPayment(rateWater, countWater) + РУБ + "\n";

        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(PAY + EXTENSION_PDF));

        document.open();

        document.add(new Paragraph(content));

        document.close();

        writer.close();

    }

    public int getDifferenceReadings(int fromOld, int toOld, int fromNew, int toNew) {
        return Integer.parseInt(readings.substring(fromNew, toNew)) - Integer.parseInt(readings.substring(fromOld, toOld));
    }


    public int getDifferenceRetes(int from, int to) {
        return Integer.parseInt(rates.substring(from, to));
    }

}

