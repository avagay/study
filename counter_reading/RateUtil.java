package homework.counter_reading;

public class RateUtil {
    public static final String PATH_TO_RATES = "./src/Rates.txt";
    public static final String PATH_TO_READINGS = "./src/Readings.txt";
    public static final String YOU_SPENT = "you spent :";
    public static final String RATE = ", Rate = ";
    public static final String РУБ = " руб ,";
    public static final String YOU_NEED_PAY = "You need pay = ";
    public static final String PAY = "Pay";
    public static final String ELECTRICITY = " electricity: ";
    public static final String GAS = " gas: ";
    public static final String WATER = " water: ";
    public static final int FROM_ELECTRICITY = 30;
    public static final int TO_ELECTRICITY = 31;
}
