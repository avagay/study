package homework.String;

/*
        1) В определенном предложении найти самое длинное и короткое слово.
        2) Перевернуть строку наоборот
        3) Подсчитать количество слов в предложении.
        http://developer.alexanderklimov.ru/android/java/string.php
*/


public class HomeWorkString {
    public static void main(String[] args) {

        String catNames = "Васькаgh Рыж Мурзик Барсикghjkk";
        /*System.out.println(catNames.length());*/ //Подсчитать количество слов в предложении.

        String[] aCats = catNames.split(" ");

        String maxCat = aCats[0];
        String minCat = aCats[0];

        for (int i = 0; i < aCats.length; i++) {
            int length = aCats[i].length();

            if (length > maxCat.length()) {
                maxCat = aCats[i];
            }

            if (length < minCat.length()) {
                minCat = aCats[i];
            }
        }
        System.out.println(maxCat + "\n" + minCat);


        //Перевернуть строку наоборот
        char[] mas = catNames.toCharArray();

        char[] newMas = new char[mas.length];

        for (int i = 0, j = mas.length - 1; i < mas.length; i++, j--) {
            newMas[j] = mas[i];
        }

       /* for (int i = 0; i < str.length/2; i++) {
            char temp = str[i];
            str[i] = str[str.length - 1 - i];
            str[str.length - 1 - i] = temp;
        }*/

        System.out.println(newMas[0]);

        catNames = String.valueOf(newMas);
        System.out.println(catNames);


    }
}
