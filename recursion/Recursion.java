package homework.recursion;

public class Recursion {

    private static int count = 0;
    public static void main(String[] args) {

       /* 7) Распечатать числа между двумя числами.

        10 и 20 - 10 11 12...20*/
//       printAB(10, 20);

        String a = String.valueOf(1847);
        int a1 = 1;
        int a2 = 2;
        int numberA = 1;
//        goNumber(a1, a2, a, numberA);
        getCountPositiveNumbers(8987);

        System.out.println(count);
    }


    public static int printAB(int a, int b) {
        System.out.println(a);
        if (a++ != b) {
            return printAB(a, b);
        }
        return 0;
    }

   /* 8)  Дано 4-х значное число (1985) найти в числе все четные цифры и количество единиц,
    вывести их на консоль и посчитать их количество.

   1887 - четные 8 и 8
   1887 - нечетные 1 и 7
    */


    public static void goNumber(int a1, int a2, String a, int numberA) {
        try {

            int number = Integer.parseInt(a.substring(a1, a2));

            if (number % 2 == 0) {
                System.out.println(number);
                System.out.println("кол-во четных чисел = " + numberA);
            }
            goNumber(++a1, ++a2, a, ++numberA);
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("This is all");
        }
    }

    public static int getCountPositiveNumbers(int a) {//198
        if ((a % 10) % 2 == 0) {//8
            ++count;
        }

        if (a < 10) {
            return 0;
        }

        return getCountPositiveNumbers(a / 10);//1
    }
}







