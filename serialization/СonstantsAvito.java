package homework.serialization;

import java.nio.file.Path;
import java.nio.file.Paths;

public class СonstantsAvito {

    public static final String WORK_DIRECTORY = "./Advertisement";
    public static final Path PATH = Paths.get(WORK_DIRECTORY);
    public static final String SCREEN = "/SCREEN_";
    public static final String PLATFORM_INFO = "/Platform.info";
    public static final String DIRECTORY_CONTAINS_UNKNOW_FILES = "Directory contains unknow files";
    public static final String TXT = ".txt";
    public static final String PLATFORM_ = "Platform_";
    public static final String PLATFORM = "/".concat(PLATFORM_);
}
