package homework.serialization;

/*
Создать приложение для связи рекламодателя и агенства по размещению рекламы.
        Рекламное агенство имеет 5 площадок с экранами для размещения рекламы, каждая с разным браузером и ОС.
        Каждая площадка - директория с пятью текстовыми файлами("экранами") и  одним файлом, в  котором сериализована
        информация о данной площадке(ось, браузер, название площадки).
        Приложение должно позволять:



        3) Создавать новую площадку.//Platform_1

        5) Удалять площадку со всей рекламой.

        1) Размещать рекламу рекламодателя только на устройствах с определенным браузером и ОСью.

        2) Менять рекламу на определенной площадке.

        6) Менять конфигурацию площадки с удалением всей рекламы.

        7) Добавлять экран на определенную площадку.

        В  приложении использовать сериализацию, корректную обработку ошибок, лямбда выражении,
        комментарии только на английском языке.


        Правки

        Создать отдельный оттдельный класс и вынести туда константы

        Написать комментарии над классом и методами
*/

import java.io.IOException;
import java.io.Serializable;

/**
 * The class work with adverts
 */


public class Avito {

    public static void main(String[] args) throws IOException {
        ServiseAvito avito = new ServiseAvito();

        avito.createDefaultPlatforms();

        /*avito.createAdvert(OS.WINDOWS, Browser.MOZILLA, "Hello8888");*/

        /*avito.changeAdvert("Platform_1", "000");*/

        /*avito.addScreen("Platform_1");*/

       /*avito.addPlatform();*/

        /*avito.remotePlatform("Platform_6");*/

        /*avito.changeConfigurationPlatform("Platform_6");*/
    }
}

class AdverPlatform implements Serializable {

    private String name;

    private OS os;

    private Browser browser;

    AdverPlatform(String name, OS os, Browser browser) {

        this.name = name;
        this.os = os;
        this.browser = browser;
    }

    public String getName() {
        return name;
    }

    public OS getOs() {
        return os;
    }

    public Browser getBrowser() {
        return browser;
    }
}


enum OS {

    WINDOWS, LINUX, MAC_OS;
}

enum Browser {

    CHROME, MOZILLA, SAFARI;

}