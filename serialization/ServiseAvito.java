package homework.serialization;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static homework.serialization.СonstantsAvito.*;

/**
 * The class work with class avito
 *
 * @author avagay
 */
public class ServiseAvito {

    private List<AdverPlatform> platforms;

    public ServiseAvito() {
        platforms = Arrays.asList(
                new AdverPlatform("Platform_1", OS.LINUX, Browser.CHROME),
                new AdverPlatform("Platform_2", OS.LINUX, Browser.MOZILLA),
                new AdverPlatform("Platform_3", OS.WINDOWS, Browser.CHROME),
                new AdverPlatform("Platform_4", OS.WINDOWS, Browser.MOZILLA),
                new AdverPlatform("Platform_5", OS.MAC_OS, Browser.SAFARI));
    }

    /**
     * The method create defaults directories
     *
     * @since 1.8
     */
    public void createDefaultPlatforms() throws IOException {

/*Проверить на сброс эксепшена*/
        if (Files.list(PATH).count() > 0 &&
                !Files.list(PATH).allMatch(dir -> dir.toString().contains(PLATFORM_))) {
            throw new IOException(DIRECTORY_CONTAINS_UNKNOW_FILES);
        } else {
            IntStream.rangeClosed(1, 5).forEach(index -> {//duplication
                try {
                    AdverPlatform adverPlatform = platforms.get(index - 1);

                    Path dir = Paths.get(WORK_DIRECTORY + "/" + adverPlatform.getName());

                    Files.createDirectory(dir);

                    createScreens(dir);

                    serializationFilePlatform(dir, adverPlatform);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

    }

    /**
     * The method create defaults screens
     *
     * @since 1.8
     */

    public void createScreens(Path dir) {

        IntStream.rangeClosed(1, 5).forEach(ix -> {
            try {
                Files.createFile(Paths.get(dir.toString() + SCREEN + ix + TXT));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * The method serialization file platform
     *
     * @since 1.8
     */

    public void serializationFilePlatform(Path path, AdverPlatform adverPlatform) throws IOException {

        try (FileOutputStream stream = new FileOutputStream(path.toString() + PLATFORM_INFO);
             ObjectOutputStream outputStream = new ObjectOutputStream(stream)) {
            outputStream.writeObject(adverPlatform);
        }
    }

    /**
     * The method deserialization file platform
     *
     * @since 1.8
     */

    public AdverPlatform deserializationFilePlatform(Path path) throws IOException, ClassNotFoundException {

        try (FileInputStream stream = new FileInputStream(String.valueOf(path));
             ObjectInputStream inputStream = new ObjectInputStream(stream)) {
            return (AdverPlatform) inputStream.readObject();
        }
    }

    /**
     * The method create advert
     *
     * @since 1.8
     */

    public void createAdvert(OS os, Browser browser, String advert) throws IOException {

        Files.list(PATH).filter(f -> {

            Path pathSer = Paths.get(f.toString() + PLATFORM_INFO);

            AdverPlatform adverPlatform = null;

            try {
                adverPlatform = deserializationFilePlatform(pathSer);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            return adverPlatform.getOs().equals(os)
                    && adverPlatform.getBrowser().equals(browser);
        }).forEach(path1 -> {
            IntStream.rangeClosed(1, 5).forEach(ix -> {//duplication
                try {
                    Files.write(Paths.get(path1.toString() + SCREEN + ix + TXT),
                            advert.getBytes(), StandardOpenOption.WRITE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });

    }

    /**
     * The method change advert on plarform
     *
     * @since 1.8
     */

    public void changeAdvert(String namePlarform, String advert) throws IOException {

        Path path = Paths.get(WORK_DIRECTORY + "\\" + namePlarform);

        IntStream.rangeClosed(1, 5).forEach(ix -> {//duplication

            try {
                Files.write(Paths.get(path.toString() + SCREEN + ix + TXT),
                        advert.getBytes(), StandardOpenOption.WRITE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * The method add screen on plarform
     * @param namePlarform name of advertisiment platform
     *
     * @throws IOException
     */

    public void addScreen(String namePlarform) throws IOException {

        Path path = Paths.get(WORK_DIRECTORY + "\\" + namePlarform);

        int count = (int) (Files.list(path).count());

        Files.createFile(Paths.get(path.toString() + SCREEN + count + TXT));

    }


    /**
     * The method create new platform
     *
     * @since 1.8
     */

    public void addPlatform() throws IOException {

        long namberPlatform = Files.list(PATH).count() + 1;

        Path dir = Paths.get(WORK_DIRECTORY + PLATFORM + namberPlatform);

        Files.createDirectory(dir);

        serializationFilePlatform(dir, new AdverPlatform(PLATFORM_ + namberPlatform, OS.MAC_OS, Browser.SAFARI));

        createScreens(dir);

    }

    /**
     * The method remote platform
     *
     * @since 1.8
     */

    public void remotePlatform(String namePlarform) throws IOException {

        Path path = Paths.get(WORK_DIRECTORY + "\\" + namePlarform);

        remoteScreens(path);

        Files.delete(path);
    }

    /**
     * The method remote screens in platform
     *
     * @since 1.8
     */

    public void remoteScreens(Path path) throws IOException {

        Files.list(path).forEach(dir -> {
            try {
                Files.delete(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * The method change configuration of platform
     *
     * @since 1.8
     */

    public void changeConfigurationPlatform(String namePlarform) throws IOException {

        Path pathPlarform = Paths.get(WORK_DIRECTORY + "\\" + namePlarform);

        remoteScreens(pathPlarform);

        serializationFilePlatform(pathPlarform, new AdverPlatform(namePlarform, OS.LINUX, Browser.CHROME));

        createScreens(pathPlarform);

    }

}
