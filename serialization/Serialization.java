package homework.serialization;

/*
Create classes Square and Rectangles with methods for calculating of perimeter and square.
Give your suggestions about relations between classes
   (is-a, has-a, use-a, etc.). Write well commented code with examples of using these classes.

   * Write code for reading and writing collections of these objects from(into) file.
   * Find object with maximal square.
   * Find object with maximal perimeter.
   * Write code for handling the incorrect format of incoming file.

Создание классов Квадрат и Прямоугольник с методами для расчета периметра и квадрата.
         Дайте свои предложения об отношениях между классами
         (is-a, has-a, use-a и т. д.). Напишите хорошо прокомментированный код с примерами использования этих классов.

         * Введите код для чтения и записи коллекций этих объектов из (в) файла.
         * Найти объект с максимальным площадью.
         * Найти объект с максимальным периметром.
         * Введите код для обработки неправильного формата входящего файла. */

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.compare;

public class Serialization {

    public static void main(String[] args) throws IOException, ClassNotFoundException,
            IllegalAccessException, InstantiationException {

        ServiceSerialization service = new ServiceSerialization();

        service.addSquareObject(10);

        service.addSquareObject(15);

        service.addRectangleObject(10, 20);

        service.addRectangleObject(10, 15);

        service.serializationListShapes();

        System.out.println(service.findMaxSquare("") + "\n" +
                service.findMaxPerimeter(""));

    }


}

class ServiceSerialization {

    private static final String FILE_NAME = "Shape.sanya";

    private List<Shape> listShape;


    ServiceSerialization() {

        listShape = new ArrayList<>();
    }


    public void addRectangleObject(int sideA, int sideB) {

        listShape.add(new Rectangle(sideA, sideB));

    }

    public void addSquareObject(int sideA) {

        listShape.add(new Square(sideA));

    }

    public void serializationListShapes() throws IOException {

        try (FileOutputStream stream = new FileOutputStream(FILE_NAME);//try-with-resource
             ObjectOutputStream outputStream = new ObjectOutputStream(stream)) {
            outputStream.writeObject(listShape);
        }

    }


    private List<Shape> deserializationListShapes(String name) {

        List<Shape> newListShape = Collections.emptyList();

        try {

            if (!FILE_NAME.equalsIgnoreCase(name)) {
                throw new IOException("Format file " + name + " isn't correct");
            }

            FileInputStream stream = new FileInputStream(FILE_NAME);

            try (ObjectInputStream inputStream = new ObjectInputStream(stream)) {//try-with-resource

                newListShape = (List<Shape>) inputStream.readObject();
            }

        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return newListShape;

    }

    public Shape findMaxSquare(String name) {

        List<Shape> newListShape = deserializationListShapes(name);

        if (newListShape.isEmpty()) {
            System.out.println("List is empty");
            return new Square(5);
        }

        Shape shape = newListShape
                .stream()
                .max((p1, p2) -> compare(p1.calculateSquare(), p2.calculateSquare()))
                .get();

        return shape;

    }

    public Shape findMaxPerimeter(String name) {

        List<Shape> newListShape = deserializationListShapes(name);

        if (newListShape.isEmpty()) {
            System.out.println("List is empty");
            return new Square(5);
        }

        Shape shape = newListShape
                .stream()
                .max((p1, p2) -> compare(p1.calculatePerimeter(), p2.calculatePerimeter()))
                .get();

        return shape;

    }


}


abstract class Shape implements Serializable {

    private int sideA;
    private int sideB;

    Shape(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    Shape(int sideA) {
        this.sideA = sideA;
    }

    public int getSideA() {
        return sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public abstract int calculatePerimeter();

    public abstract int calculateSquare();


}

class Rectangle extends Shape {

    Rectangle(int sideA, int sideB) {
        super(sideA, sideB);
    }

    public int calculatePerimeter() {
        return (getSideA() + getSideB()) * 2;
    }

    public int calculateSquare() {
        return getSideA() * getSideB();
    }

}


class Square extends Shape {

    Square(int a) {
        super(a);
    }

    public int calculatePerimeter() {
        return 4 * getSideA();
    }

    public int calculateSquare() {
        return getSideA() * getSideA();
    }


}
