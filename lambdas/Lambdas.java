package homework.lambdas;

/*3) Создать имитацию запросов в базу данных на основе коллекции c применением lambdas.

        Сущности Man(имя, фамилия, возраст, количество детей, Адрес), Адрес(страна, город, улица, количество жителей)

        "SELECT * FROM Man";

        "SELECT * FROM Address";

        "SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";

        "UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)

        "SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";

        "SELECT count(*) FROM Address GROUP BY city, nameOfStreet";

        "SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";

        "SELECT count(*) FROM Man GROUP BY city, nameOfStreet";

        "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 10";*/

import com.sun.org.apache.xpath.internal.functions.Function;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;


public class Lambdas {

    private List<Man> men;

    public Lambdas() {
        men = new ArrayList<>();

        Man man_1 = new Man("Sany", "Bozuluk", 30, 3,
                new Address("US", "Toglity", "Marksa", 500000));

        Man man_2 = new Man("Igor", "Chernov", 30, 2,
                new Address("Canada", "Moskow", "Lenina", 6000000));

        Man man_3 = new Man("Igor", "Chernov", 30, 1,
                new Address("Canada", "Moskow", "Lenina", 6000000));

        Man man_4 = new Man("Igor", "Chernov", 30, 1,
                new Address("Canada", "Moskow", "Lenina", 6000000));

        men.add(man_1);
        men.add(man_2);
        men.add(man_3);
        men.add(man_4);

    }

    public List<Man> getMen() {
        return men;
    }

    public void selectBySpecifyOptions(Consumer<Man> consumer) {
        men.stream().forEach(consumer);
    }

    public void selectByPredicateAndConsumer(Predicate<Man> predicate, Consumer<Man> consumer) {
        men.stream().filter(predicate).forEach(consumer);
    }


    public static void main(String[] args) {

        Lambdas lambdas = new Lambdas();

        List<Man> men = lambdas.getMen();

        /*"SELECT * FROM Man"*/

        Consumer<Man> consumer = System.out::println;

        lambdas.selectBySpecifyOptions(consumer);

        /*"SELECT * FROM Address"*/

        consumer = man -> System.out.println(man.getAddress().getStreet());

        lambdas.selectBySpecifyOptions(consumer);

        /*"SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName"*/

        Predicate<Man> predicate = man -> man.getAge() >= 20;

        consumer = man -> System.out.println("firstName = " + man.getFirstName()
                + ", lastName = " + man.getSecondName()
                + ", countOfChildren = " + man.getNumberChildren());

        Comparator<Man> manComparator = (o1, o2) -> o1.getFirstName().compareTo(o2.getFirstName());

        manComparator.reversed();

        men.stream().filter(predicate)
                .sorted(manComparator)
                .forEach(consumer);

        lambdas.selectByPredicateAndConsumer(predicate, consumer);


        /*"UPDATE Man SET firstName = 'John', lastName = 'Kennedi', " +
                "countOfChildren = 3 WHERE country == 'US' (or another country)*/

       /* Predicate<Man> byCountry = man -> man.getAddress().getCountry().equals("US");

        Consumer<Man> manConsumer = man -> {
            man.setFirstName("John");
            man.setSecondName("Kennedi");
            man.setNumberChildren(3);
            System.out.println("firstName = " + man.getFirstName());
        };

        lambdas.selectByPredicateAndConsumer(byCountry, manConsumer);*/

        /*"SELECT firstName, lastName, nameOfStreet FROM Man " +
                "WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";*/

        predicate = man -> (man.getAddress().getCountry().equals("Canada")
                && man.getNumberChildren() == 3) || man.getAge() >= 25;

        consumer = man -> System.out.println(man.getFirstName());

        lambdas.selectByPredicateAndConsumer(predicate, consumer);



        /*"SELECT count(*) FROM Man GROUP BY countOfChildren"*/


        java.util.function.Function<Man, Integer> getNumberChildren = man -> man.getNumberChildren();

        Map<Integer, Long> collect = men
                .stream().
                        collect(Collectors.groupingBy(getNumberChildren, Collectors.counting()));

        System.out.println(collect);

        /*"SELECT count(*) FROM Man GROUP BY countOfChildren, age"*/

        /*Map<Integer, Map<Integer, List<Man>>> collectMans = men.stream()
                .collect(Collectors.groupingBy(Man::getNumberChildren,
                        Collectors.groupingBy(Man::getAge), Collectors.counting()));
        */

        Map<AbstractMap.SimpleEntry<Integer, Integer>, Long> collect1 = men.stream()
                .collect(Collectors.groupingBy(man ->
                        new AbstractMap.SimpleEntry<>(man.getNumberChildren(), man.getAge()), Collectors.counting()));


        System.out.println("Grouping by children and age -> " + collect1);

//        "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 2";*/

        Map<AbstractMap.SimpleEntry<String, String>, Long> collect2 = men.stream().collect(Collectors.groupingBy(man ->
                        new AbstractMap.SimpleEntry<>(man.getAddress().getCity(), man.getAddress().getStreet()),
                Collectors.counting()));

        Set<Map.Entry<AbstractMap.SimpleEntry<String, String>, Long>> entries = collect2.entrySet();

        long count = entries.stream().filter(entry -> entry.getValue() > 2).count();

        System.out.println(count);

//        "SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 10";

        List<Address> collect3 = men.stream().map(Man::getAddress).collect(toList());

    }


}

class Man {

    private String firstName;

    private String secondName;

    private int age;

    private int numberChildren;

    private Address address;

    Man(String firstName, String secondName, int age, int numberChildren, Address address) {

        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.numberChildren = numberChildren;
        this.address = address;

    }

    public Address getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getNumberChildren() {
        return numberChildren;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setNumberChildren(int numberChildren) {
        this.numberChildren = numberChildren;
    }

    @Override
    public String toString() {
        return "Man{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", age=" + age +
                ", numberChildren=" + numberChildren +
                ", address=" + address +
                '}';
    }
}

class Address {

    private String country;

    private String city;

    private String street;

    private int numberPeople;


    Address(String country, String city, String street, int numberPeople) {

        this.country = country;
        this.city = city;
        this.street = street;
        this.numberPeople = numberPeople;

    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getNumberPeople() {
        return numberPeople;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", numberPeople=" + numberPeople +
                '}';
    }
}

