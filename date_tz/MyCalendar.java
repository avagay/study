package homework.date_tz;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

import static java.time.format.TextStyle.FULL;

/*В отдельной ветке + тесты

    Организовать приложение календарь.
    Приложение должно позволять:
   1) Выводить текущее время и дату в нескольких тайм-зонах(на выбор).
    При выводе даты и времени выводить также события на данный день.
   2) Создавать событие на конкретную дату и по требованию пользователя выводить список событий.
    Добавить возможность удалять события.
   3) Позволять пользователю вводить свой город(страну), определить его тайм-зону и выводить текущее время,
    день недели в этой тайм-зоне.
   4) По требованию пользователя выводить в консоль дату через неделю, месяц, год.
   5) По требованию пользователя выводить в консоль только время и/или дату, день недели, номер дня в году,
    количество дней оставшеееся до Нового Года.
   6) Позволить пользователю вводить формат даты и выводить дату в этом формате.*/

public class MyCalendar { // make own type exception

    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) throws CalendarException {

        CalendarService operationDate = new CalendarService();
        int operation;

        do {
            System.out.println("Display the current time and date in several time zones - 1 \n" +
                    "Create an name for date (example 2007-12-03) - 2 \n" +
                    "Display events - 3 \n" +
                    "Delete name - 4 \n" +
                    "Display the current date with time zones for city - 5 \n" +
                    "Display the current time - 6 \n" +
                    "Display the current date - 7 \n" +
                    "Display the current day of week - 8 \n" +
                    "Display the current number day in year - 9 \n" +
                    "Display the current number day before new year - 10 \n" +
                    "Display format of date: yyyy dd MM   - 11 \n" +
                    "Display format of date: MM yyyy dd  - 12 \n" +
                    "Display the date in a week - 13 \n" +
                    "Display the date in a month - 14 \n" +
                    "Display the date in a year - 15 \n" +
                    "exit - 0");
            operation = SCANNER.nextInt();

            switch (operation) {
                case 1:
                    /*Отображение текущего времени и даты в нескольких часовых поясах*/
                    operationDate.displayDifferentTimeZones();

                    break;
                case 2:
                    /*Создание события на дату со временем*/

                        String newEvent = SCANNER.next();

                        String newDate = SCANNER.next();

                        operationDate.createAndAddEvent(newEvent, newDate);


                    break;
                case 3:
                    /*Показать события*/
                    operationDate.displayEvents();

                    break;
                case 4:
                    String nameEvent = SCANNER.next();
                    operationDate.removeEvents(nameEvent);
                    /*Удалить событие*/

                    break;
                case 5:
                    operationDate.displayDifferentTimeZones();
                    /*Отображение текущей даты с часовыми поясами для города*/

                    break;
                case 6:

                    operationDate.displayCurrentTime();
                    /*Отображение текущего времени*/

                    break;
                case 7:
                    System.out.println(operationDate.displayCurrentDate());
                    /*Показывать текущую дату*/

                    break;
                case 8:
                    System.out.println(operationDate.displayDateOfWeek());
                    /*Отобразить текущий день недели*/

                    break;
                case 9:
                    System.out.println(operationDate.displayDateOfYear());
                    /*Показывать текущий день в году*/

                    break;
                case 10:
                    System.out.println(operationDate.displayDateOfNewYear());
                    /*Показывать текущий номер дня до нового года*/

                    break;
                case 11:
                    System.out.println(operationDate.displayDateFormatter("yyyy dd MM"));
                     /*Выберите формат даты и вывести*/

                    break;
                case 12:
                    System.out.println(operationDate.displayDateFormatter("MM yyyy dd"));
                    /*Выберите формат даты и вывести*/

                    break;

                case 13:
                    System.out.println(operationDate.displayDatePlus("Week"));
                    /*Показывать дату через неделю*/

                    break;

                case 14:
                    System.out.println(operationDate.displayDatePlus("Month"));
                     /*Показывать дату через месяц*/

                    break;

                case 15:
                    System.out.println(operationDate.displayDatePlus("Year"));
                     /*Показывать дату через год*/

                    break;

            }

        }
        while (operation != 0);
    }


}


class Event {

    private String name;

    private LocalDate date;


    public LocalDate getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    Event(String name, LocalDate date) {
        this.name = name;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", date=" + date +
                '}';
    }


}


class CalendarService {

    public static final DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ISO_LOCAL_TIME;

    public static final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ISO_LOCAL_DATE;

    private ArrayList<Event> events;

    public CalendarService() {
        this.events = new ArrayList<>();
    }

    public ArrayList<Event> getEvents() {

        return events;
    }

    public void setEvents(ArrayList<Event> events) {

        this.events = events;
    }


    public void displayDifferentTimeZones() {

        System.out.println("Current time and date for Toglitty = " + LocalDateTime.now(ZoneId.of("Europe/Samara")) +

                "\nCurrent time and date for Kiev = " + LocalDateTime.now(ZoneId.of("Europe/Kiev")) +

                "\nCurrent time and date for Moscow = " + LocalDateTime.now(ZoneId.of("Europe/Moscow")));

    }

    public void createAndAddEvent(String newEvent, String newDate) {

        try {
            Event eventDate = new Event(newEvent, LocalDate.parse(newDate));

            events.add(eventDate);
        }
        catch (DateTimeParseException e) {
            try {
                throw new CalendarException();
            } catch (CalendarException newE) {
                System.out.println("Wrong format date. Try again");
            }
        }


    }


    public void displayEvents() {

        events.stream().forEach(System.out::println);

    }

    public boolean removeEvents(String nameEvent) {

        for (Event event : events) {

            if (event.getName().equals(nameEvent)) {

                return events.remove(event);
            }
        }
        return false;
    }

    public String displayCurrentTime() {

        LocalDateTime date = LocalDateTime.now();

        return date.format(FORMATTER_TIME);
    }

    public String displayCurrentDate() {

        LocalDate date = LocalDate.now();

        return date.format(FORMATTER_DATE);
    }

    public String displayDateOfWeek() {

        LocalDate date = LocalDate.now();

        return date.getDayOfWeek().getDisplayName(FULL, new Locale("Russian"));

    }

    public int displayDateOfYear() {

        return LocalDate.now().getDayOfYear();
    }

    public int displayDateOfNewYear() {

        LocalDate date = LocalDate.now();

        return date.lengthOfYear() - date.getDayOfYear();
    }


    public String displayDateFormatter(String check) {

        LocalDate date = LocalDate.now();

        DateTimeFormatter formatter;

        switch (check) {
            case "y d M":
                formatter = DateTimeFormatter.ofPattern("yyyy dd MM");
                break;

            case "M y d":
                formatter = DateTimeFormatter.ofPattern("MM yyyy dd");
                break;

            default:
                formatter = FORMATTER_TIME;
        }

        return date.format(formatter);

    }

    public LocalDate displayDatePlus(String check) {

        LocalDate date = LocalDate.now();

        switch (check) {

            case "Year":
                return date.plusYears(1);

            case "Week":
                return date.plusWeeks(1);

            case "Month":
                return date.plusMonths(1);
        }

        return date;
    }
}


class CalendarException extends Exception {


    public CalendarException() {
    }

}



