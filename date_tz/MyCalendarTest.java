package homework.date_tz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalendarService.class)

public class MyCalendarTest {

    public static final String NEW_EVENT = "newEvent";
    public static final String NEW_DATE = "2007-12-03";
    public static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2000, 11, 2, 3, 15);
    public static final LocalDate LOCAL_DATE = LocalDate.of(2000, 1, 2);
    public static final LocalDate LOCAL_DATE_PLUS_YEAR = LocalDate.of(2001, 1, 2);
    public static final LocalDate LOCAL_DATE_PLUS_WEEK = LocalDate.of(2000, 1, 9);
    public static final LocalDate LOCAL_DATE_PLUS_MONTH = LocalDate.of(2000, 2, 2);
    public static final String YEAR = "Year";
    public static final String WEEK = "Week";
    public static final String MONTH = "Month";

    @Rule
    public final SystemOutRule OUTRULE = new SystemOutRule().enableLog();

    private CalendarService calendarService = new CalendarService();

    @Mock
    private CalendarService calendarServiceMock;

    @Spy
    private CalendarService calendarServiceSpy = new CalendarService();


    @Before
    public void beforeEachTest() {

        calendarService.getEvents().clear();
    }


    @Test
    public void shouldCreateAndAddEvent() {

        calendarService.createAndAddEvent(NEW_EVENT, NEW_DATE);

        Event event = calendarService.getEvents().get(0);

        Assert.assertEquals(NEW_EVENT, event.getName());

        Assert.assertEquals(LocalDate.parse(NEW_DATE), event.getDate());

    }


    @Test
    public void shouldDisplayDifferentTimeZones() {

        PowerMockito.mockStatic(LocalDateTime.class);

        Mockito.when(LocalDateTime.now(ZoneId.of("Europe/Samara"))).thenReturn(LOCAL_DATE_TIME);

        Mockito.when(LocalDateTime.now(ZoneId.of("Europe/Kiev"))).thenReturn(LOCAL_DATE_TIME);

        Mockito.when(LocalDateTime.now(ZoneId.of("Europe/Moscow"))).thenReturn(LOCAL_DATE_TIME);

        calendarServiceSpy.displayDifferentTimeZones();

        String log = OUTRULE.getLogWithNormalizedLineSeparator();

        Assert.assertTrue(log.contains("Toglitty = 2000-11-02T03:15"));

    }


        @Test
        public void shouldDisplayEvents () {

            calendarService.createAndAddEvent(NEW_EVENT, NEW_DATE);

            calendarService.displayEvents();

            String log = OUTRULE.getLogWithNormalizedLineSeparator();

            Assert.assertTrue(log.contains(NEW_EVENT));

            Assert.assertTrue(log.contains(NEW_DATE));

        }

        @Test
        public void shouldRemoveEvents () {

            calendarService.createAndAddEvent(NEW_EVENT, NEW_DATE);

            Assert.assertFalse(calendarService.getEvents().isEmpty());

            calendarService.removeEvents(NEW_EVENT);

            Assert.assertTrue(calendarService.getEvents().isEmpty());
        }

        @Test
        public void shouldDisplayCurrentTime () {

            PowerMockito.mockStatic(LocalDateTime.class);

            Mockito.when(LocalDateTime.now()).thenReturn(LOCAL_DATE_TIME);

            Mockito.when(calendarServiceMock.displayCurrentTime()).thenCallRealMethod();

            Assert.assertEquals("03:15:00", calendarServiceMock.displayCurrentTime());

            Assert.assertTrue("03:15:00".equals(calendarServiceMock.displayCurrentTime()));
        }

        @Test
        public void shouldDisplayCurrentDate () {

            PowerMockito.mockStatic(LocalDate.class);

            Mockito.when(LocalDate.now()).thenReturn(LOCAL_DATE);

            Mockito.when(calendarServiceMock.displayCurrentDate()).thenCallRealMethod();

            Assert.assertEquals("2000-01-02", calendarServiceMock.displayCurrentDate());
        }

        @Test
        public void shouldDisplayDateOfWeek () {

            PowerMockito.mockStatic(LocalDate.class);

            Mockito.when(LocalDate.now()).thenReturn(LOCAL_DATE);

            Mockito.when(calendarServiceMock.displayDateOfWeek()).thenCallRealMethod();

            Assert.assertEquals("Sunday", calendarServiceMock.displayDateOfWeek());
        }

        @Test
        public void shouldDisplayDateOfYear () {

            Mockito.doReturn(2).when(calendarServiceMock).displayDateOfYear();

            Assert.assertEquals(2, calendarServiceMock.displayDateOfYear());

        }

        @Test
        public void shouldDisplayDateOfNewYear () {

            PowerMockito.mockStatic(LocalDate.class);

            Mockito.when(LocalDate.now()).thenReturn(LOCAL_DATE);

            Mockito.when(calendarServiceMock.displayDateOfNewYear()).thenCallRealMethod();

            Assert.assertEquals(364, calendarServiceMock.displayDateOfNewYear());

        }

        @Test
        public void shouldDisplayDateFormatter () {

            PowerMockito.mockStatic(LocalDate.class);

            Mockito.when(LocalDate.now()).thenReturn(LOCAL_DATE);

            Mockito.when(calendarServiceMock.displayDateFormatter("y d M")).thenCallRealMethod();

            Mockito.when(calendarServiceMock.displayDateFormatter("M y d")).thenCallRealMethod();

            Assert.assertEquals("2000 02 01", calendarServiceMock.displayDateFormatter("y d M"));

            Assert.assertEquals("01 2000 02", calendarServiceMock.displayDateFormatter("M y d"));

        }

        @Test
        public void shouldDisplayDatePlus () {

            PowerMockito.mockStatic(LocalDate.class);

            Mockito.when(LocalDate.now()).thenReturn(LOCAL_DATE);

            Mockito.when(calendarServiceMock.displayDatePlus(YEAR)).thenCallRealMethod();

            Mockito.when(calendarServiceMock.displayDatePlus(WEEK)).thenCallRealMethod();

            Mockito.when(calendarServiceMock.displayDatePlus(MONTH)).thenCallRealMethod();

            Assert.assertEquals(LOCAL_DATE_PLUS_YEAR, calendarServiceMock.displayDatePlus(YEAR));

            Assert.assertEquals(LOCAL_DATE_PLUS_WEEK, calendarServiceMock.displayDatePlus(WEEK));

            Assert.assertEquals(LOCAL_DATE_PLUS_MONTH, calendarServiceMock.displayDatePlus(MONTH));

        }
    }
