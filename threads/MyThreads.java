package homework.threads;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class MyThreads {



    public static void main(String[] args) throws InterruptedException {

        ServiceThreads service = new ServiceThreads();

        Runnable runnable = service::operationRunnable;

        Thread workerOne = new Thread(runnable);

        Thread workerTwo = new Thread(runnable);

        Thread workerThree = new Thread(runnable);

        Thread workerFour = new Thread(runnable);

        Thread workerFive = new Thread(runnable);

        System.out.println("Start one");

        workerOne.start();

        workerOne.join();

        System.out.println("Start two");

        workerTwo.start();

        workerTwo.join();

        workerThree.start();

        System.out.println("Start three");

        workerThree.join();

        workerFour.start();

        workerFour.join();

        workerFive.start();

        workerFive.join();
    }

}


class ServiceThreads {

    final static String WORK_DIRECTORY = "./Story.txt";

    public final static String SEARCH_WORD = "holidays";

    public static final Path PATH = Paths.get(WORK_DIRECTORY);

    public void operationRunnable(){

        long start = System.currentTimeMillis();

        String content = null;
        try {
            content = new String(Files.readAllBytes(PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] contens = content.split(" ");

        String wordFinded = Arrays.stream(contens)
                .filter(word -> word.equals(SEARCH_WORD))
                .findFirst()
                .get();

        long finish = System.currentTimeMillis() - start;

        System.out.println("We are finded words! This is = " + wordFinded +
                ", is'nt it?" + " Worker " + Thread.currentThread().getName() +
                " Time spent = " + finish);

    }
}

