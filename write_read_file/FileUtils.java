package homework.write_read_file;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {
    public static final String REGEX = "I'm ready for writting to file";
    public static final String REPLACEMENT = "I'm from file";
    public static final Path PATH_TO_FILE = Paths.get("./src/Poem.txt");

}
