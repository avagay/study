
package homework.write_read_file.write;


/*Создать абстрактный класс родителя Writer c методом modifyText, который реализует интерфейс Writable
        c абстрактными методом write. Создать потомка, который будет писать данные в текстовый файл Poem.txt.
        Данные вводит юзер с консоли. К  данным введенным юзером в методе modifyText добавлять строку
        "I'm ready for writting to file".
        */


// пишем в файл с помощью Files


import homework.write_read_file.reader.DescendantReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

import static homework.write_read_file.FileUtils.PATH_TO_FILE;

public abstract class Writer implements Writable {


    public abstract String modifyText(String data);

}

class DescendantWriter extends Writer {


    @Override
    public void write(String data) throws IOException {
        data = modifyText(data);
        Files.write(PATH_TO_FILE, data.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Override
    public String modifyText(String data) {
        return data.concat(" I'm ready for writting to file");
    }


    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);

        DescendantWriter writer = new DescendantWriter();

        String data = scan.nextLine();

        writer.write(data);

        DescendantReader reader = new DescendantReader();

        data = reader.read();
        System.out.println(data);
    }

}
