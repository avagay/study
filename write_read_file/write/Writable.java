package homework.write_read_file.write;

import java.io.IOException;

public interface Writable {
    void write(String data)throws IOException;
}
