
package homework.write_read_file.write;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DemoWriter {
    public static void main(String[] args) throws IOException{
        String data = "I'm ready for writting to file";

        writeUsingFiles(data);
    }


    private static void writeUsingFiles(String data) throws IOException {
        Files.write(Paths.get("./src/Poem.txt"), data.getBytes());

    }
}

