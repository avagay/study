package homework.write_read_file.reader;

import java.io.IOException;

public interface Readable {
    String read() throws IOException;
}
