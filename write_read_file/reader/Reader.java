package homework.write_read_file.reader;


/*Создать абстрактный класс родителя Reader c методом modifyText, который реализует интерфейс Readable c абстрактным
        методом read. Создать потомка, который будет читать данные из текстового файла Poem.txt и вывести данные в консоль.
        Перед выводом в консоль в методе modifyText заменить строку  "I'm ready for writting to file" на "I'm from file".
        использовать класс Files*/

import homework.write_read_file.write.Writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Reader implements Readable {

    public abstract String modifyText(String data);

}