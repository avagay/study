package homework.write_read_file.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static homework.write_read_file.FileUtils.*;

public class DescendantReader extends Reader {



    @Override
    public String modifyText(String data) {
       /* Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(data);
        return matcher.replaceAll(REPLACEMENT);*/
       return data.replaceAll(REGEX, REPLACEMENT);
    }

    @Override
    public String read() throws IOException {
//        String data = new String(Files.readAllBytes(Writer.PATH_TO_FILE));
        List<String> strings = Files.readAllLines(PATH_TO_FILE);
        String line = null;

        StringBuilder builder = new StringBuilder();

        try (BufferedReader bufferedReader = Files.newBufferedReader(PATH_TO_FILE)) {
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
        }

        return modifyText(builder.toString());
    }

}
